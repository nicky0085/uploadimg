
import React, { Component } from 'react';
import './App.css';

//Import npm bulma
import 'bulma/css/bulma.css'

//Import npm react-filepond
import { FilePond, File, registerPlugin } from 'react-filepond';

// Import FilePond styles
import 'filepond/dist/filepond.min.css';

// FilePond Register plugin
import FilePondImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
registerPlugin(FilePondImagePreview);

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
        files: [], //upload file 
    };
}

  handleInit() {
    // handle init file upload here
    console.log('now initialised', this.pond);
  }

  handleProcessing(fieldName, file, metadata, load, error, progress, abort) {
    // handle file upload here
    console.log(" handle file upload here");
    console.log(file);
  }

  render() {
    return (
      <div className="App">
      <div className="Margin-25">

          {/* Pass FilePond properties as attributes */}
          <FilePond allowMultiple={true}
                  maxFiles={3}
                  ref= {ref => this.pond = ref}
                  server={{ process: this.handleProcessing.bind(this) }}
                  oninit={() => this.handleInit()}>    
                  
              {/* Set current files using the <File/> component */}
              {this.state.files.map(file => (
                <File key={file} source={file} />
              ))}

          </FilePond>

      </div>
  </div>
    );
  }
}

export default App;